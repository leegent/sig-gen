# We build up in two stages - stage one installs and uses pipenv to retrieve deps
# stage two is the runtime and doesn't contain pipenv -- only contains deps
FROM python:3.10.12-alpine AS builder
WORKDIR /app
ENV PIPENV_VENV_IN_PROJECT=1
RUN pip install --user pipenv==2023.7.4
ADD Pipfile Pipfile.lock /app/
RUN /root/.local/bin/pipenv sync  # installs packages specified in Pipfile.lock

FROM python:3.10.12-alpine AS runtime
WORKDIR /app
ENV PYTHONPATH=.
RUN mkdir -v /app/.venv
COPY --from=builder /app/.venv/ /app/.venv
ADD . .
ENV PORT 80
CMD ["sh", "-c", "./.venv/bin/gunicorn -t 300 -b 0.0.0.0:$PORT app:app"]
EXPOSE ${PORT}
