from flask import Flask,  render_template
from forms import SignatureForm
from generate_image import ImageGenerator
from models import ImagePayload

app = Flask(__name__)
app.config.from_object('config')


@app.route('/', methods=['GET', 'POST'])
def sig_form():
    form = SignatureForm()
    if form.validate_on_submit():
        image_input = ImagePayload(form)

        image_generator = ImageGenerator(image_input)
        sig_image = image_generator.generate_image()
        return render_template('form.html', image=sig_image, form=form, image_width=form.image_width.data)

    return render_template('form.html', form=form)


if __name__ == '__main__':
    app.run()
