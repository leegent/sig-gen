from flask_wtf.file import FileRequired, FileField, FileAllowed
from flask_wtf import FlaskForm
from wtforms import StringField, validators
from wtforms.fields.numeric import IntegerField


class SignatureForm(FlaskForm):
    name = StringField('Name', [validators.Length(max=100)], default="Sam Gent")
    job_title = StringField('Job Title', [validators.Length(max=100)], default="IT Manager")

    line_1 = StringField('Address Line 1', [validators.Length(max=200)], default="McKeag & Co Solicitors | 1-3 Lansdowne Terrace | Gosforth")
    line_2 = StringField('Address Line 2', [validators.Length(max=200)], default="Newcastle upon Tyne | NE3 1HN | DX 60353 Gosforth")

    tel = StringField('Tel', [validators.Length(max=50)], default="0191 213 1010")
    fax = StringField('Fax', [validators.Length(max=50)], default="0191 213 1704")
    web = StringField('Web', [validators.Length(max=50)], default="www.mckeagandco.com")

    twitter = StringField('Twitter', [validators.Length(max=50)], default="@McKeag2131010")
    facebook = StringField('Facebook', [validators.Length(max=50)], default="/mckeagandco")

    holiday_message = StringField('Holiday Message (optional)', [validators.Length(max=100)])

    image = FileField("Photo Thumbnail", validators=[FileRequired(), FileAllowed(['jpg', 'png', 'gif'], 'Images only!')])
    holiday_image = FileField("Holiday Message Image (optional)",
                      validators=[FileAllowed(['jpg', 'png', 'gif'], 'Images only!')])
    image_width = IntegerField('Generated Sig Image Width', [validators.NumberRange(min=300, max=1000)], default=792)
    image_height = IntegerField('Generated Sig Height', [validators.NumberRange(min=100, max=1000)], default=239)
    bg_colour = StringField('Background Colour', default="ffffff")
    text_colour = StringField('Text Colour', default="000000")
