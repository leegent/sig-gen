import base64
from io import BytesIO
from typing import Tuple

from PIL import Image, ImageDraw, ImageFont
from werkzeug.datastructures.file_storage import FileStorage

from models import ImagePayload


class ImageGenerator:
    RENDER_SCALE = 10
    POST_LINE_PADDING = 0.2  # in line heights

    TEXT_SECTION_V_GUTTER_PERCENT = 0.035
    TEXT_SECTION_H_GUTTER_PERCENT = 0.035

    PICTURE_SECTION_V_GUTTER_PERCENT = 0.035
    PICTURE_SECTION_H_GUTTER_PERCENT = 0.040
    PICTURE_WIDTH_PERCENT = 0.25
    HOLIDAY_IMAGE_WIDTH_PERCENT = 0.05

    def __init__(self, image_spec: ImagePayload):
        self.image_spec = image_spec

        self.FINAL_IMAGE_HEIGHT = image_spec.image_height
        self.FINAL_IMAGE_WIDTH = image_spec.image_width

        self.IMAGE_HEIGHT = self.FINAL_IMAGE_HEIGHT * self.RENDER_SCALE
        self.IMAGE_WIDTH = self.FINAL_IMAGE_WIDTH * self.RENDER_SCALE

        self.TEXT_SECTION_H_GUTTER = int(self.TEXT_SECTION_H_GUTTER_PERCENT * self.IMAGE_WIDTH)
        self.TEXT_SECTION_V_GUTTER = int(self.TEXT_SECTION_V_GUTTER_PERCENT * self.IMAGE_WIDTH)
        self.PICTURE_SECTION_V_GUTTER = int(self.PICTURE_SECTION_V_GUTTER_PERCENT * self.IMAGE_WIDTH)
        self.PICTURE_SECTION_H_GUTTER = int(self.PICTURE_SECTION_H_GUTTER_PERCENT * self.IMAGE_WIDTH)
        self.THUMBNAIL_PICTURE_WIDTH = int(self.PICTURE_WIDTH_PERCENT * self.IMAGE_WIDTH) - (2 * self.PICTURE_SECTION_V_GUTTER)
        self.HOLIDAY_IMAGE_WIDTH = int(self.HOLIDAY_IMAGE_WIDTH_PERCENT * self.IMAGE_WIDTH)

        self.name_font = ImageFont.truetype("fonts/Noto_Sans/static/NotoSans-SemiBold.ttf", 20 * self.RENDER_SCALE)
        self.title_font = ImageFont.truetype("fonts/Noto_Sans/static/NotoSans-Regular.ttf", 18 * self.RENDER_SCALE)
        self.main_font = ImageFont.truetype("fonts/Noto_Sans/static/NotoSans-Regular.ttf", 15 * self.RENDER_SCALE)
        self.contact_font = ImageFont.truetype("fonts/Noto_Sans/static/NotoSans-Regular.ttf", 13 * self.RENDER_SCALE)
        self.holiday_font = ImageFont.truetype("fonts/Noto_Sans/static/NotoSans-Italic.ttf", 13 * self.RENDER_SCALE)

    def generate_image(self) -> str:
        img = Image.new("RGBA", (self.IMAGE_WIDTH, self.IMAGE_HEIGHT), self.image_spec.bg_colour)
        draw = ImageDraw.Draw(img)

        thumbnail_bb = self.square_scale_and_add_image_file(
            int(self.PICTURE_SECTION_H_GUTTER),
            int(self.PICTURE_SECTION_V_GUTTER),
            int(self.THUMBNAIL_PICTURE_WIDTH),
            self.image_spec.photo_data, img, draw)
        thumbnail_right = thumbnail_bb[0][0]
        thumbnail_bottom = thumbnail_bb[1][1]

        # Insert socials bit
        # We will resize the source images to be the height of a line/char
        twitter_icon = Image.open("icons/twitter.png")
        line_height = self.line_height(draw, self.image_spec.twitter, self.main_font)
        twitter_icon_resized = twitter_icon.resize((line_height, line_height))
        yt = thumbnail_bottom + int(line_height * self.POST_LINE_PADDING)
        img.alpha_composite(twitter_icon_resized, (thumbnail_right, yt))
        yt = self.add_text(draw, thumbnail_right + int(line_height*1.1), yt, self.image_spec.twitter, self.main_font)

        yt = int(yt)
        facebook_icon = Image.open("icons/facebook.png")
        facebook_icon_resized = facebook_icon.resize((line_height, line_height))
        img.alpha_composite(facebook_icon_resized, (thumbnail_right, yt))
        yt = self.add_text(draw, thumbnail_right + int(line_height*1.1), yt, self.image_spec.facebook, self.main_font)

        # Text area: x offset is Pic H gutter + pic width + text secion h gutter
        txt_area_x_offset = self.PICTURE_SECTION_H_GUTTER + self.THUMBNAIL_PICTURE_WIDTH + self.TEXT_SECTION_H_GUTTER
        yt = self.TEXT_SECTION_V_GUTTER
        yt = self.add_text(draw, txt_area_x_offset, yt, self.image_spec.name, self.name_font)
        yt = self.add_text(draw, txt_area_x_offset, yt, self.image_spec.job_title, self.title_font)
        yt = self.add_text(draw, txt_area_x_offset, yt, " ", self.main_font)  # TODO: don't do this please
        yt = self.add_text(draw, txt_area_x_offset, yt, self.image_spec.line_1, self.main_font)
        yt = self.add_text(draw, txt_area_x_offset, yt, self.image_spec.line_2, self.main_font)

        # Assemble contact line
        contact = f"T {self.image_spec.tel} | F {self.image_spec.fax} | W {self.image_spec.web}"
        yt = self.add_text(draw, txt_area_x_offset, yt, contact, self.main_font)

        if self.image_spec.holiday_image:
            holiday_image_bb = self.square_scale_and_add_image_file(txt_area_x_offset, yt, self.HOLIDAY_IMAGE_WIDTH, self.image_spec.holiday_image, img, draw)
            holiday_image_right = holiday_image_bb[1][0]
            holiday_text_x_offset = holiday_image_right + int(self.HOLIDAY_IMAGE_WIDTH * 0.2)
            holiday_text_y_offset = yt + int(self.HOLIDAY_IMAGE_WIDTH / 2.0) - int(line_height/2.0)
            yt = self.add_multiline_text(draw, holiday_text_x_offset, holiday_text_y_offset, self.image_spec.holiday_message,
                                         self.holiday_font)
        else:
            yt = self.add_text(draw, txt_area_x_offset, yt, " ", self.main_font)  # TODO: don't do this please
            yt = self.add_multiline_text(draw, txt_area_x_offset, yt, self.image_spec.holiday_message, self.holiday_font)

        # Scale down now using supersampling
        final_image = img if self.RENDER_SCALE == 1 else img.resize((self.FINAL_IMAGE_WIDTH, self.FINAL_IMAGE_HEIGHT), Image.LANCZOS)
        img_io = BytesIO()
        final_image.save(img_io, 'PNG')
        img_io.seek(0)
        return str(base64.b64encode(img_io.getvalue()), 'utf-8')
        # return send_file(img_io, mimetype='image/png')

    def line_height(self, draw: ImageDraw.ImageDraw, t: str, font: ImageFont.FreeTypeFont) -> int:
        bb: tuple[int, int, int, int] = draw.textbbox((0,0), t, font)
        return bb[3]

    def add_text(self, draw: ImageDraw.ImageDraw, x: int, y: int, t: str, font: ImageFont.FreeTypeFont) -> int:
        bb: tuple[int, int, int, int] = draw.textbbox((0,0), t, font)
        draw.text((x, y), t, self.image_spec.text_colour, font)
        return y + int(bb[3] * (1.0 + self.POST_LINE_PADDING))

    def add_multiline_text(self, draw: ImageDraw.ImageDraw, x: int, y: int, t: str, font: ImageFont.FreeTypeFont) -> int:
        bb: tuple[int, int, int, int] = draw.textbbox((0,0), t, font)
        draw.multiline_text((x, y), t, self.image_spec.text_colour, font)
        offset = int(bb[3] * (1.0 + self.POST_LINE_PADDING))
        return y + offset

    @classmethod
    def square_scale_and_add_image_file(cls, left: int, top: int, required_side_length: int, image_file: FileStorage, img: Image, draw: ImageDraw) -> Tuple[Tuple[int, int],Tuple[int, int]]:
        # Image area:
        img_area_x_offset = left
        img_area_y_offset = top

        # Insert thumbnail square
        photo = Image.open(image_file.stream)
        photo = photo.convert("RGBA")
        photo_w = photo.width
        photo_h = photo.height
        width_biggest = photo_w > photo_h
        scale_ratio = required_side_length / photo_w if width_biggest else required_side_length / photo_h
        new_w = int(photo_w * scale_ratio)
        new_h = int(photo_h * scale_ratio)
        photo_x_offset = img_area_x_offset + (0 if width_biggest else int((required_side_length-new_w)/2.0))
        photo_y_offset = img_area_y_offset + (int((required_side_length-new_h)/2.0) if width_biggest else 0)
        resized = photo.resize((new_w, new_h))
        img.alpha_composite(resized, (photo_x_offset, photo_y_offset))
        img_end_x = int(photo_x_offset + new_w)
        img_end_y = int(photo_y_offset + new_h)

        bb = ((photo_x_offset, photo_y_offset),(img_end_x, img_end_y))

        draw.rectangle(bb, None, "darkgrey")

        return bb
