from dataclasses import dataclass
from typing import Optional
from werkzeug.datastructures.file_storage import FileStorage

from forms import SignatureForm


@dataclass
class ImagePayload:
    name: str
    job_title: str
    line_1: str
    line_2: str
    tel: str
    fax: str
    web: str
    twitter: str
    facebook: str
    photo_data: FileStorage
    holiday_image: Optional[FileStorage]
    holiday_message: Optional[str]
    image_width: int
    image_height: int
    bg_colour: str
    text_colour: str

    def __init__(self, validated_form: SignatureForm):
        # Why have a class that looks exactly like another class?
        # Because I'm not a savage
        # and I'd like to be able to drive/test the image generator without 'forms'
        self.name = validated_form.name.data
        self.job_title = validated_form.job_title.data
        self.line_1 = validated_form.line_1.data
        self.line_2 = validated_form.line_2.data
        self.tel = validated_form.tel.data
        self.fax = validated_form.fax.data
        self.web = validated_form.web.data
        self.twitter = validated_form.twitter.data
        self.facebook = validated_form.facebook.data
        self.photo_data = validated_form.image.data
        self.image_width = validated_form.image_width.data
        self.image_height = validated_form.image_height.data
        self.bg_colour = f"#{validated_form.bg_colour.data}"
        self.text_colour = f"#{validated_form.text_colour.data}"
        self.holiday_message = validated_form.holiday_message.data
        self.holiday_image = validated_form.holiday_image.data
